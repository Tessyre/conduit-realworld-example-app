import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../../context/AuthContext';
import userSignUp from '../../services/userSignUp';
import FormFieldset from '../FormFieldset';

function SignUpForm({ onError }) {
  const [{ username, email, password }, setForm] = useState({
    username: '',
    email: '',
    password: '',
  });
  const { setAuthState } = useAuth();
  const navigate = useNavigate();

  const handleSubmit = e => {
    e.preventDefault();

    userSignUp({ username, email, password })
      .then(setAuthState)
      .then(() => navigate('/'))
      .catch(onError);
  };

  const inputHandler = e => {
    const { name } = e.target;
    const { value } = e.target;

    setForm(form => ({ ...form, [name]: value }));
  };

  return (
    <form onSubmit={handleSubmit}>
      <FormFieldset
        name="username"
        required
        placeholder="Your Name"
        value={username}
        handler={inputHandler}
      />

      <FormFieldset
        name="email"
        type="email"
        required
        placeholder="Email"
        value={email}
        handler={inputHandler}
      />

      <FormFieldset
        name="password"
        type="password"
        required
        placeholder="Password"
        value={password}
        handler={inputHandler}
      />
      <button className="btn btn-lg btn-primary pull-xs-right">Sign up</button>
    </form>
  );
}

export default SignUpForm;
